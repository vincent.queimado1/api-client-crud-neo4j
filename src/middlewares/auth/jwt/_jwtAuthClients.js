const jwt = require('jsonwebtoken');

const config = require('config/server');
const httpMsg = require('src/utils/http_handler/http_msg');
const userExist = require('src/api/services/users/findOne');

const conf = config[process.env.NODE_ENV || 'development'];

module.exports = async (req) => {
    let user;

    let token = await getTokenFromHeader(req);
    if (!token) return (httpMsg.http401('Invalid token'));
    
    let verified = await verifyToken(token);
    if (verified.error) return (httpMsg.http401('Invalid token'));
    
    if (verified.payload) {
        user = { 
            id: verified.payload.id, 
            username: verified.payload.username, 
            email: verified.payload.email,
        }
    }

    let exist = await userExist(user);
    if (!exist.data) return (httpMsg.http401('Invalid token'));
    
    return ({ success: true, data: user });
}

const verifyToken = (token) => {
    let result = jwt.verify(token, conf.session.secret, (err, decoded) => {
        if (err) return ({ error: err.message || err, payload: null });
        return ({ error: null, payload: decoded });
      })
    return result;
}

const getTokenFromHeader = (req) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token' ||
        req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }

    return null;
}
