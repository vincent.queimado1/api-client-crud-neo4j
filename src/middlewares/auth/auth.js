const authJwt = require('src/middlewares/auth/jwt')

module.exports = (req, res, next) => {
    authJwt.jwtClients(req)
        .then(result => {
            if(!result.success){
            res.status(result.httpStatusCode).json(result.data)
            } else {
            req.user = result.data
            next()
            }
        })
        .catch((err) => { console.log(err); next(err) })
}