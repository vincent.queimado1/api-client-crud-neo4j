const schema = require('src/middlewares/validation/schemas');
const httpMsg = require('src/utils/http_handler/http_msg');

const userSignin = (req, res, next) => {
  const { error } = schema.usersAuth.signin.validate(req.body, { abortEarly: false, errors: {wrap: { label: false } } });
  if(error) {
    let err = httpMsg.http422('Validation error', error.details[0].message || null);
    return res.status(err.httpStatusCode).json(err.data);
  }
  return next();
}

const userSignup = (req, res, next) => {
  const { error } = schema.usersAuth.signup.validate(req.body, { abortEarly: false, errors: {wrap: { label: false } } });
  if(error) {
    let err = httpMsg.http422('Validation error', error.details[0].message || null);
    return res.status(err.httpStatusCode).json(err.data);
  }
  return next();
}

const userMe = (req, res, next) => {
  const { error } = schema.users.readMe.validate(req.user, { abortEarly: false, errors: {wrap: { label: false } } });
  if(error) {
    let err = httpMsg.http422('Validation error', error.details[0].message || null);
    return res.status(err.httpStatusCode).json(err.data);
  }
  return next();
}

const userCreate = (req, res, next) => {
  const { error } = schema.users.create.validate(req.body, { abortEarly: false, errors: { wrap: { label: false } } });
  if(error) {
    let err = httpMsg.http422('Validation error', error.details[0].message || null);
    return res.status(err.httpStatusCode).json(err.data);
  }
  return next();
}

const userRead = (req, res, next) => {
  const { error } = schema.users.read.validate(req.query, { abortEarly: false, errors: {wrap: { label: false } } });
  if(error) {
    let err = httpMsg.http422('Validation error', error.details[0].message || null);
    return res.status(err.httpStatusCode).json(err.data);
  }
  return next();
}

const userUpdate = (req, res, next) => {
    const { error } = schema.users.update.validate(req.body, { abortEarly: false, errors: { wrap: { label: false } } });
    if(error) {
        let err = httpMsg.http422('Validation error', error.details[0].message || null);
        return res.status(err.httpStatusCode).json(err.data);
      }
    return next();
}

const userDelete = (req, res, next) => {
  const { error } = schema.users.del.validate(req.query, { abortEarly: false, errors: {wrap: { label: false } } });
  if(error) {
    let err = httpMsg.http422('Validation error', error.details[0].message || null);
    return res.status(err.httpStatusCode).json(err.data);
  }
  return next();
}

module.exports = {
    userSignup,
    userSignin,
    userCreate,
    userRead,
    userUpdate,
    userDelete,
    userMe
}
