const Joi = require('joi')

const create = Joi.object({
    username: Joi
    .string()
    .regex(/^[A-Za-z0-9\s]*$/)
    .min(2)
    .max(64)
    .required()
    .messages({
      'string.pattern.base': `Opa! digite um nome válido e tente novamente.`,
      'string.empty': `Opa! digite seu nome e tente novamente.`,
      'string.min': `Opa! seu nome deve conter no mínimo {#limit} caracteres.`,
      'string.max': `Opa! seu nome deve conter no máximo {#limit} caracteres.`,
      'any.required': `Opa! digite seu nome e tente novamente.`
    }),
    email: Joi
    .string()
    .email()
    .max(128)
    .lowercase()
    .required()
    .messages({
      'string.email': `Opa! parece que esse e-mail é inválido. Por favor, tente novamente.`,
      'string.empty': `Opa! digite seu e-mail e tente novamente!`,
      'string.lowecase': `Opa! digite seu e-mail com letras maiúsculas!`,
      'string.max': `Opa! seu e-mail deve conter no mínimo {#limit} caracteres.`,
      'any.required': `Opa! digite seu e-mail e tente novamente!`
    }),
    password: Joi
    .string()
    .regex(/^[A-Za-z0-9!@#$%&]*$/)
    .min(4)
    .max(10)
    .required()
    .messages({
      'string.empty': `Opa! digite sua senha e tente novamente!`,
      'string.pattern.base': `Opa! parece que sua senha é inválida!`,
      'string.min': `Opa! sua senha deve conter no mínimo {#limit} caracteres!`,
      'string.max': `Opa! sua senha deve conter no máximo {#limit} caracteres!`,
      'any.required': `Opa! digite sua senha e tente novamente!`
    })
})
.messages({
  'object.unknown': 'Opa! campo {{#label}} não permitido!'
})

const read = Joi.object({
  id: Joi
  .number()
  .integer()
  .required()
  .messages({
    'any.required': `Opa! id inválido!`
  })
})

const readMe = Joi.object({
  id: Joi
  .number()
  .integer()
  .required()
  .messages({
    'any.required': `Opa! id inválido!`
  }),
  username: Joi
  .string()
  .regex(/^[A-Za-z0-9\s]*$/)
  .min(2)
  .max(64)
  .messages({
    'string.pattern.base': `Opa! digite um nome válido e tente novamente.`,
    'string.empty': `Opa! digite seu nome e tente novamente.`,
    'string.min': `Opa! seu nome deve conter no mínimo {#limit} caracteres.`,
    'string.max': `Opa! seu nome deve conter no máximo {#limit} caracteres.`,
    'any.required': `Opa! digite seu nome e tente novamente.`
  }),
  email: Joi
  .string()
  .email()
  .max(128)
  .lowercase()
  .messages({
    'string.email': `Opa! parece que esse e-mail é inválido. Por favor, tente novamente.`,
    'string.empty': `Opa! digite seu e-mail e tente novamente!`,
    'string.lowecase': `Opa! digite seu e-mail com letras maiúsculas!`,
    'string.max': `Opa! seu e-mail deve conter no mínimo {#limit} caracteres.`,
    'any.required': `Opa! digite seu e-mail e tente novamente!`
  }),
})

const update = Joi.object({
  username: Joi
  .string()
  .regex(/^[A-Za-z0-9\s]*$/)
  .min(2)
  .max(64)
  .messages({
    'string.pattern.base': `Opa! digite um nome válido e tente novamente.`,
    'string.empty': `Opa! digite seu nome e tente novamente.`,
    'string.min': `Opa! seu nome deve conter no mínimo {#limit} caracteres.`,
    'string.max': `Opa! seu nome deve conter no máximo {#limit} caracteres.`,
    'any.required': `Opa! digite seu nome e tente novamente.`
  }),
  email: Joi
  .string()
  .email()
  .max(128)
  .lowercase()
  .messages({
    'string.email': `Opa! parece que esse e-mail é inválido. Por favor, tente novamente.`,
    'string.empty': `Opa! digite seu e-mail e tente novamente!`,
    'string.lowecase': `Opa! digite seu e-mail com letras maiúsculas!`,
    'string.max': `Opa! seu e-mail deve conter no mínimo {#limit} caracteres.`,
    'any.required': `Opa! digite seu e-mail e tente novamente!`
  }),
  password: Joi
  .string()
  .regex(/^[A-Za-z0-9!@#$%&]*$/)
  .min(4)
  .max(10)
  .messages({
    'string.empty': `Opa! digite sua senha e tente novamente!`,
    'string.pattern.base': `Opa! parece que sua senha é inválida!`,
    'string.min': `Opa! sua senha deve conter no mínimo {#limit} caracteres!`,
    'string.max': `Opa! sua senha deve conter no máximo {#limit} caracteres!`,
    'any.required': `Opa! digite sua senha e tente novamente!`
  })
})
.messages({
  'object.unknown': 'Opa! campo {{#label}} não permitido!'
})  

const del = Joi.object({
  id: Joi
  .string()
  .required()
  .messages({
    'string.empty': `Opa! id inválido!`,
    'any.required': `Opa! id inválido!`
  })
})

module.exports = { 
  create,
  read,
  update,
  del,
  readMe
}