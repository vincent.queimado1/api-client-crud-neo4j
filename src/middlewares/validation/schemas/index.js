const users = require('src/middlewares/validation/schemas/_users');
const usersAuth = require('src/middlewares/validation/schemas/_usersAuth');

module.exports = {
    users,
    usersAuth
}