const Joi = require('joi')

const signup = Joi.object({
    username: Joi
    .string()
    .min(2)
    .max(64)
    .required()
    .messages({
      'string.pattern.base': `Opa! digite um nome válido e tente novamente.`,
      'string.empty': `Opa! digite seu nome e tente novamente.`,
      'string.min': `Opa! seu nome deve conter no mínimo {#limit} caracteres.`,
      'string.max': `Opa! seu nome deve conter no máximo {#limit} caracteres.`,
      'any.required': `Opa! digite seu nome e tente novamente.`
    }),
    email: Joi
    .string()
    .email()
    .max(128)
    .required()
    .messages({
      'string.email': `Opa, seu e-mail inválido!`,
      'string.empty': `Opa, o usuário é obrigatório!`,
      'string.max': `Opa, o e-mail deve conter no mínimo {#limit} caracteres!`,
      'any.required': `Opa, o usuário é obrigatório!`
    }),
    password: Joi
    .string()
    .regex(/^[A-Za-z0-9!@#$%&]*$/)
    .min(6)
    .max(16)
    .required()
    .messages({
      'string.empty': `Opa, a senha é obrigatório!`,
      'string.pattern.base': `Opa, senha inválida!`,
      'string.min': `Opa, a senha deve conter no mínimo {#limit} caracteres!`,
      'string.max': `Opa, a senha deve conter no máximo {#limit} caracteres!`,
      'any.required': `Opa, a senha é obrigatória!`
    })
  })
  .messages({
    'object.unknown': 'Opa! campo {{#label}} não permitido!'
  })
  .with('username', 'email')

  const signin = Joi.object({
    email: Joi
    .string()
    .email()
    .max(128)
    .required()
    .messages({
      'string.email': `Opa, seu e-mail inválido!`,
      'string.empty': `Opa, o usuário é obrigatório!`,
      'string.max': `Opa, o e-mail deve conter no mínimo {#limit} caracteres!`,
      'any.required': `Opa, o usuário é obrigatório!`
    }),
    password: Joi
    .string()
    .regex(/^[A-Za-z0-9!@#$%&]*$/)
    .min(6)
    .max(16)
    .required()
    .messages({
      'string.empty': `Opa, a senha é obrigatório!`,
      'string.pattern.base': `Opa, senha inválida!`,
      'string.min': `Opa, a senha deve conter no mínimo {#limit} caracteres!`,
      'string.max': `Opa, a senha deve conter no máximo {#limit} caracteres!`,
      'any.required': `Opa, a senha é obrigatória!`
    })
  })
  .messages({
    'object.unknown': 'Opa! campo {{#label}} não permitido!'
  })
  .with('email', 'password')

module.exports = { 
  signup,
  signin
}