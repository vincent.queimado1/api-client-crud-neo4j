const presenter = require('src/api/presenters/commons')

exports.root = (req, res, next) => {
  presenter.apiRoot(req.query)
    .then(result => {
      res.redirect(result.data.content)
    })
    .catch(err => next(err))
}

exports.info = (req, res, next) => {
  presenter.apiInfo(req.query)
    .then(result => res.status(result.httpStatusCode).json(result.data))
    .catch(err => next(err))
}