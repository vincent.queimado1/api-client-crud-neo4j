const presenter = require('src/api/presenters/usersAuth');

exports.signup = (req, res, next) => {
  presenter.signup(req.body)
    .then(result => res.status(result.httpStatusCode).json(result.data))
    .catch(err => next(err))
}
  
exports.signin = (req, res, next) => {
  presenter.signin(req.body)
      .then(result => res.status(result.httpStatusCode).json(result.data))
      .catch(err => next(err))
}
