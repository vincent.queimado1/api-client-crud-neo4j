const presenter = require('src/api/presenters/users');

exports.findAll = (req, res, next) => {
  presenter.findAll()
    .then(result => res.status(result.httpStatusCode).json(result.data))
    .catch(err => next(err))
}

exports.findOne = (req, res, next) => {
  presenter.findOne(req.query)
    .then(result => res.status(result.httpStatusCode).json(result.data))
    .catch(err => next(err))
}

exports.create = (req, res, next) => {
  presenter.create(req.body)
    .then(result => res.status(result.httpStatusCode).json(result.data))
    .catch(err => next(err))
}
  
exports.update = (req, res, next) => {
  presenter.update(req.query, req.body)
    .then(result => res.status(result.httpStatusCode).json(result.data))
    .catch(err => next(err))
}

exports.delete = (req, res, next) => {
  presenter.remove(req.query)
    .then(result => res.status(result.httpStatusCode).json(result.data))
    .catch(err => next(err))
}
