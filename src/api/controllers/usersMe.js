const presenter = require('src/api/presenters/usersMe');
  
exports.readMe = (req, res, next) => {
    presenter.readMe(req.user)
        .then(result => res.status(result.httpStatusCode).json(result.data))
        .catch(err => next(err))
}

exports.updateMe = (req, res, next) => {
    presenter.updateMe(req.user, req.body)
        .then(result => res.status(result.httpStatusCode).json(result.data))
        .catch(err => next(err))
}

exports.deleteMe = (req, res, next) => {
    presenter.deleteMe(req.user)
        .then(result => res.status(result.httpStatusCode).json(result.data))
        .catch(err => next(err))
}
  