const query = require('src/database/cql/usersgroups');
const logger = require('src/utils/logging/winston/logger');
const neo4j = require('src/database/db_driver/neo4j/neo4jSession');

module.exports = async (datas) => { 
  try {
    let result = await neo4j.executeCypherAsync(query.create(datas));
    if (!result || result.records.length === 0) return ({ success: true, data: null, error: 'Erro ao tentar criar usuário.' });

    return ({ success: true, data: result, error: null });
  } catch (err) {
    logger.error(`Erro ao tentar criar usuário. ${err.message}`);
    return ({ success: false, data: null, error: err });
  }
}
