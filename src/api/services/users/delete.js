const query = require('src/database/cql/users');
const logger = require('src/utils/logging/winston/logger');
const neo4j = require('src/database/db_driver/neo4j/neo4jSession');

module.exports = async (id) => { 
  try {
    let result = await neo4j.executeCypherAsync(query.remove(id));
    if (!result || result.records.length === 0) return ({ success: true, data: null, error: 'Nenhum usuário encontrado.' });

    return ({ success: true, data: result, error: null });
  } catch (err) {
    logger.error(`Erro ao tentar deletar usuário. ${err.message}`);
    return ({ success: false, data: null, error: err });
  }
}
