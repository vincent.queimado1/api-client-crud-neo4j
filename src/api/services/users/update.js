const query = require('src/database/cql/users');
const logger = require('src/utils/logging/winston/logger');
const neo4j = require('src/database/db_driver/neo4j/neo4jSession');

module.exports = async (id, datas) => { 
  try {
    let result = await neo4j.executeCypherAsync(query.update(id, datas));
    if (!result || result.records.length === 0) return ({ success: true, data: null, error: 'Erro ao tentar editar usuário.' });

    return ({ success: true, data: result, error: null });
  } catch (err) {
    logger.error(`Erro ao tentar editar usuário. ${err.message}`);
    return ({ success: false, data: null, error: err });
  }
}
