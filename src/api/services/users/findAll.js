const User = require('src/api/models/users');
const query = require('src/database/cql/users');
const logger = require('src/utils/logging/winston/logger');
const neo4j = require('src/database/db_driver/neo4j/neo4jSession');

module.exports = async () => { 
  try {
    let users =[];

    let result = await neo4j.executeCypherAsync(query.findAll());
    if (!result || result.records.length === 0) return ({ success: true, data: null, error: 'Users not found.' });

    if(result.records.length > 0) {
      for (const record of result.records) {
        let user = new User(record.get('u'));
        users.push({ ...user.toJson() })
      }
    }

    return ({ success: true, data: users, qty: result.records.length, error: null });
  } catch (err) {
    logger.error(`Error to find users. ${err.message}`);
    return ({ success: false, data: null, error: err });
  }
}