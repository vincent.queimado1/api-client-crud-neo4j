const User = require('src/api/models/users');
const query = require('src/database/cql/users');
const logger = require('src/utils/logging/winston/logger');
const neo4j = require('src/database/db_driver/neo4j/neo4jSession');

module.exports = async (datas) => { 
  try {
    let result = await neo4j.executeCypherAsync(query.create(datas));
    if (!result || result.records.length === 0) return ({ success: true, data: null, error: 'Erro ao tentar criar Usuario.' });

    if(result.records[0]) {
      let user = new User(result.records[0].get('u'));
      return ({ success: true, data: { ...user.toJson() }, error: null });
    }

    return ({ success: true, data: null, error: null });
  } catch (err) {
    logger.error(`Erro ao tentar criar Usuario. ${err.message}`);
    return ({ success: false, data: null, error: err });
  }
}
