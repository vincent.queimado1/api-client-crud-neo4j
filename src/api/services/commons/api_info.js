const pkg = require('package.json')

module.exports = () => {
    let data = { 'name': pkg.name, 'description': pkg.description, 'version': pkg.version }
    return ({ success: true, data, error: null })
}