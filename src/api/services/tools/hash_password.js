const bcrypt = require('bcrypt')
const config = require('config/server')

const rounds = config[process.env.NODE_ENV || 'development'].session.rounds

const hash = async(password) => {
  let saltRounds = parseInt(rounds)

  if (!password) { return ({ success: false, data: null, error: 'Password data missing' }) }

  let hashPassword = bcrypt.hashSync(password, saltRounds)
  
  if (!bcrypt.compareSync(password, hashPassword)) { 
    return ({ success: false, data: null, error: 'Invalid Password' }) 
  }

  return ({ success: true, data: hashPassword, error: null })
}

const check = async(password, hashPassword) => {
    if (!password || !hashPassword) { return ({ success: false, data: null, error: 'Password data missing' }) }
  
    if (!bcrypt.compareSync(password, hashPassword)) { 
      return ({ success: false, data: null, error: 'Invalid password' }) 
    }
  
    return ({ success: true, data: password, error: null })
  }

module.exports = {
    hash,
    check
}