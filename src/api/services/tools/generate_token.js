const jwt = require('jsonwebtoken')
const config = require('config/server')

const conf = config[process.env.NODE_ENV || 'development']

module.exports = (datas) => {
  let token = jwt.sign({ 
    id: datas.id, 
    username: datas.username, 
    email: datas.email,  
    }, 
    conf.session.secret, 
    { 
      expiresIn: conf.jwt.expiredIn 
    })

  return ({ success: true, data: token, error: null })
}