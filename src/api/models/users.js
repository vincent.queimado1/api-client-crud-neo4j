module.exports = class Users {

    constructor(node, following) {
        this.node = node
        this.following = following
    }

    getId(){
        return this.node.properties.id
    }

    getPassword() {
        return this.node.properties.password
    }

    toJson() {
        const { password, ...properties } = this.node.properties
        
        return {
            id: this.node.identity,
            ...properties,
            following: this.following
        }
    }

}