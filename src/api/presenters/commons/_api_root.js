const httpMsg = require('src/utils/http_handler/http_msg')
const apiRoot = require('src/api/services/commons/api_root')

const lblErr = 'Root error'

module.exports = async () => {
  let result = await apiRoot()

  if (!result.success || !result.data) return (httpMsg.http422('Erro ao redirecionar', lblErr))

  return (httpMsg.http200(result.data))
}