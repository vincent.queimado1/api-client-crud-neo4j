const apiRoot = require('./_api_root')
const apiInfo = require('./_api_info')

module.exports = {
  apiRoot,
  apiInfo
}
