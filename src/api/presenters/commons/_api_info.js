const httpMsg = require('src/utils/http_handler/http_msg')
const apiInfo = require('src/api/services/commons/api_info')

const lblErr = 'API Info error'

module.exports = async () => {
  let result = await apiInfo()

  if (!result.success || !result.data) return (httpMsg.http422('Erro ao carregar informações', lblErr))

  return (httpMsg.http200(result.data))
}
