const httpMsg = require('src/utils/http_handler/http_msg')
const findAll = require('src/api/services/usersGroups/findAll')

const lblErr = 'Find error'

module.exports = async () => {
  let usergroups = [];

  let usergroupsFindAll = await findAll();
  if (!usergroupsFindAll.success) return (httpMsg.http422('Error to find User Groups.', lblErr));
 
  if(usergroupsFindAll.data) {
    for (const usergroup of usergroupsFindAll.data.records) {
      let { properties, identity } = usergroup._fields[0];
      usergroups.push({
        'id': identity, 
        'name': properties.name
      });
    }
  }

  return (httpMsg.http200({ usergroups, qty: usergroups.length }));
}
