const httpMsg = require('src/utils/http_handler/http_msg')
const del = require('src/api/services/usersGroups/delete')

const lblErr = 'Delete error'

module.exports = async (datas) => {
  let usergroup = [];

  if(!datas.id) return ({ success: false, data: null, error: 'User Group ID is required.' });

  let usergroupDelete = await del(datas.id);
  if (!usergroupDelete.success) return (httpMsg.http422('Error to delete User Group.', lblErr));
  if (!usergroupDelete.data) return (httpMsg.http422('User Group not found.', lblErr));

  if(usergroupDelete.data) {
    let { properties, identity } = usergroupDelete.data.records[0]._fields[0];

    usergroup.push({
      'id': identity, 
      'name': properties.name
    });
  }

  return (httpMsg.http200({ usergroup }))
}
