const httpMsg = require('src/utils/http_handler/http_msg')
const findOne = require('src/api/services/usersGroups/findOne')

const lblErr = 'Find error'

module.exports = async (datas) => {
  let usergroup = [];
  let dtUsergroup = {};

  datas.id ? dtUsergroup.id = datas.id : dtUsergroup.id = '';
  datas.name ? dtUsergroup.name = datas.name : dtUsergroup.name = '';

  let usergroupFindOne = await findOne(dtUsergroup);
  if (!usergroupFindOne.success) return (httpMsg.http422('Error to find User group.', lblErr));

  if(usergroupFindOne.data) {
    let { properties, identity } = usergroupFindOne.data.records[0]._fields[0];

    usergroup.push({
      'id': identity, 
      'name': properties.name
    });
  }
  

  return (httpMsg.http200({ usergroup, qty: usergroup.length }))
}
