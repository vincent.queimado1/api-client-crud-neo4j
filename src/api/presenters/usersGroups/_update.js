const httpMsg = require('src/utils/http_handler/http_msg');
const update = require('src/api/services/usersGroups/update');

const lblErr = 'Update error';

module.exports = async (params, datas) => {
  let usergroup = [];
  let dtUsergroup = {};

  if (datas.name) dtUsergroup.name = datas.name;

  let usergroupUpdate = await update(params.id, dtUsergroup);
  if (!usergroupUpdate.success) return (httpMsg.http422('Error to edit User Group.', lblErr));

  if(usergroupUpdate.data) {
    let { properties, identity } = usergroupUpdate.data.records[0]._fields[0];

    usergroup.push({
      'id': identity, 
      'name': properties.name
    });
  }

  return (httpMsg.http201({ usergroup }))
}