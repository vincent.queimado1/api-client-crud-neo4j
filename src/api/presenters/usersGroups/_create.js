const httpMsg = require('src/utils/http_handler/http_msg');
const create = require('src/api/services/usersGroups/create');

const lblErr = 'Create error';

module.exports = async (datas) => {
  let usergroup = [];
  let dtUsergroup = {};

  datas.id ? dtUsergroup.id = datas.id : dtUsergroup.id = '';
  datas.name ? dtUsergroup.name = datas.name : dtUsergroup.name = '';

  let usergroupCreate = await create(dtUsergroup);
  if (!usergroupCreate.success) return (httpMsg.http422('Error to create User Group.', lblErr));

  if(usergroupCreate.data) {
    let { properties, identity } = usergroupCreate.data.records[0]._fields[0];

    usergroup.push({
      'id': identity,
      'name': properties.name,
    });
  }

  return (httpMsg.http201({ usergroup }))
}