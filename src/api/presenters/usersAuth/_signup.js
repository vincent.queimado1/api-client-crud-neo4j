const httpMsg = require('src/utils/http_handler/http_msg');
const userCreate = require('src/api/services/users/create');
const userFindOne = require('src/api/services/users/findOne');
const pwdCheck = require('src/api/services/tools/hash_password');

const lblErr = 'Signup error'

module.exports = async (datas) => {
  let user = {};

  let userExist = await userFindOne(datas)
  if (!userExist.success) return (httpMsg.http422('Registration error.', lblErr ));
  if (userExist.data) return (httpMsg.http422('User email already exist.', lblErr ));

  let hashedPwd = await pwdCheck.hash(datas.password);
  if (!hashedPwd.success) return (httpMsg.http422('Registration error.', lblErr ));

  datas.password = hashedPwd.data;

  let userCreated = await userCreate(datas);
  if (!userCreated.success || !userCreated.data) return (httpMsg.http422('Registration error.', lblErr ));

  if(userCreated.data) {
    user.id = userCreated.data.id;
    user.username = userCreated.data.username;
    user.email = userCreated.data.email;
  }

  return (httpMsg.http201(user));
}