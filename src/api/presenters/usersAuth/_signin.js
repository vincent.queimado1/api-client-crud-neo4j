const httpMsg = require('src/utils/http_handler/http_msg');
const userFindOne = require('src/api/services/users/findOne');
const pwdCheck = require('src/api/services/tools/hash_password');
const genToken = require('src/api/services/tools/generate_token');

const lblErr = 'Signin error';

module.exports = async (datas) => {
  let user = {};

  let userExist = await userFindOne(datas)

  if (!userExist.success) return (httpMsg.http422('Login error.', lblErr ))
  if (!userExist.data) return (httpMsg.http422('Invalid user or password.', lblErr ))

  let checkedPwd = await pwdCheck.check(datas.password, userExist.password);
  if (!checkedPwd.success) return (httpMsg.http422('Invalid user or password.', lblErr ));
  
  let token = await genToken(userExist.data);
  if (!token.success) return (httpMsg.http401('Error to signin.', lblErr ));

  if(userExist.data) {
    user.id = userExist.data.id;
    user.username = userExist.data.username;
    user.email = userExist.data.email;
    user.token = token.data;
  }

  return (httpMsg.http200(user));
}

