const httpMsg = require('src/utils/http_handler/http_msg');
const update = require('src/api/services/users/update');

const lblErr = 'Update error';

module.exports = async (params, datas) => {
  let user = [];
  let dtUser = {};

  if (datas.name) dtUser.name = datas.name;
  if (datas.email) dtUser.email = datas.email;

  let userUpdate = await update(params.id, dtUser);
  if (!userUpdate.success) return (httpMsg.http422('Error to edit User', lblErr));

  if(userUpdate.data) {
    let { properties, identity } = userUpdate.data.records[0]._fields[0];

    user.push({
      'id': identity, 
      'name': properties.name
    });
  }

  return (httpMsg.http201({ user }))
}