const httpMsg = require('src/utils/http_handler/http_msg');
const create = require('src/api/services/users/create');

const lblErr = 'Create error';

module.exports = async (datas) => {
  let user = [];
  let dtUser = {};

  datas.id ? dtUser.id = datas.id : dtUser.id = '';
  datas.name ? dtUser.name = datas.name : dtUser.name = '';
  datas.email ? dtUser.email = datas.email : dtUser.email = '';

  let userCreate = await create(dtUser);
  if (!userCreate.success) return (httpMsg.http422('Error to create User.', lblErr));

  if(userCreate.data) {
    let { properties, identity } = userCreate.data.records[0]._fields[0];

    user.push({
      'id': identity,
      'name': properties.name,
      'email': properties.email
    });
  }

  return (httpMsg.http201({ user }))
}