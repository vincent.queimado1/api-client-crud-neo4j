const httpMsg = require('src/utils/http_handler/http_msg')
const findOne = require('src/api/services/users/findOne')

const lblErr = 'Find error'

module.exports = async (datas) => {
  let user = {};
  let dtUser = {};
console.log(datas)
  if (datas.id) dtUser.id = datas.id;
  if (datas.username) dtUser.username = datas.useername;
  if (datas.email) dtUser.email = datas.email;

  if(!Object.keys(dtUser).length) return (httpMsg.http422('User data is required.', lblErr));

  let userFindOne = await findOne(dtUser);
  if (!userFindOne.success) return (httpMsg.http422('Error to find User.', lblErr));
  console.log(userFindOne )

  if(userFindOne.data) {
    user.id = userFindOne.data.id;
    user.username = userFindOne.data.username;
    user.email = userFindOne.data.email;
  }
  

  return (httpMsg.http200(user))
}
