const httpMsg = require('src/utils/http_handler/http_msg')
const findAll = require('src/api/services/users/findAll')

const lblErr = 'Find error'

module.exports = async () => {
  let usersFindAll = await findAll();
  if (!usersFindAll.success) return (httpMsg.http422('Error to find Users', lblErr));

  return (httpMsg.http200({ users: usersFindAll.data, qty: usersFindAll.qty }));
}
