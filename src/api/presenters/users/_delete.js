const httpMsg = require('src/utils/http_handler/http_msg')
const del = require('src/api/services/users/delete')

const lblErr = 'Delete error'

module.exports = async (datas) => {
  let user = [];

  if(!datas.id) return ({ success: false, data: null, error: 'User ID is required.' });

  let userDelete = await del(datas.id);
  if (!userDelete.success) return (httpMsg.http422('Error to delete User.', lblErr));
  if (!userDelete.data) return (httpMsg.http422('User not found.', lblErr));

  if(userDelete.data) {
    let { properties, identity } = userDelete.data.records[0]._fields[0];

    user.push({
      'id': identity, 
      'name': properties.name,
      'email': properties.email
    });
  }

  return (httpMsg.http200({ user }))
}
