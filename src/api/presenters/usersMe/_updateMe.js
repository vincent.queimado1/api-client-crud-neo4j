const httpMsg = require('src/utils/http_handler/http_msg');
const userUpdate = require('src/api/services/users/update');
const userFindOne = require('src/api/services/users/findOne');
const pwdCheck = require('src/api/services/tools/hash_password');

module.exports = async (params, datas) => {

  if(datas.email !== params.email){
    let userExist = await userFindOne(datas)
    if (!userExist.success) return (httpMsg.http422('Erro ao atualizar usere', 'Update user error' ))
    if (userExist.data) return (httpMsg.http422('E-mail de usere já existe', 'Update user error'))
  }

  let hashedPwd = await pwdCheck.hash(datas.password)
  if (!hashedPwd.success) return (httpMsg.http422('Erro ao atualizar usere', 'Update user error' ))

  datas.id = params.id
  datas.password = hashedPwd.data
  
  let userUpdated = await userUpdate(datas)
  if (!userUpdated.success || !userUpdated.data) return (httpMsg.http422('Error ao atualizar usere', 'Update user error'))

  return (httpMsg.http200(userUpdated.data))
}
