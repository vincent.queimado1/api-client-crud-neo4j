const readMe = require('./_readMe')
const updateMe = require('./_updateMe')
const deleteMe = require('./_deleteMe')

module.exports = {
  readMe,
  updateMe,
  deleteMe
}
