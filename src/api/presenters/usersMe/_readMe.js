const httpMsg = require('src/utils/http_handler/http_msg')
const findOne = require('src/api/services/users/findOne')

const lblErr = 'Find error'

module.exports = async (datas) => {
  let user = {};
  let dtUser = {};

  console.log(datas)

  datas.id ? dtUser.id = datas.id : dtUser.id = '';

  let userFindOne = await findOne(dtUser);
  if (!userFindOne.success) return (httpMsg.http422('Error to find User ', lblErr));

  if(userFindOne.data) {
    user.id = userFindOne.data.id;
    user.username = userFindOne.data.username;
    user.email = userFindOne.data.email;
  }

  return (httpMsg.http200(user));
}