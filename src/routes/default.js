const express = require('express');
const CtrlCommons = require('src/api/controllers/commons');

const router = express.Router();

// Cors Settings
router.all('/*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
  })

// API Redirects
router.get('/', CtrlCommons.root);
router.get('/api', CtrlCommons.root);
router.get(`/api/v1`, CtrlCommons.root);

// API Info
router.get(`/api/v1/info`, CtrlCommons.info);

module.exports = router;