const express = require('express');

const CtrlUsers = require('src/api/controllers/users');
const CtrlUsersMe = require('src/api/controllers/usersMe');
const CtrlUsersAuth = require('src/api/controllers/usersAuth');
const CtrlUsersGroups = require('src/api/controllers/usersGroups');
const validator = require('src/middlewares/validation/validator');
const auth = require('src/middlewares/auth/auth');

const router = express.Router();

// USER AUTH (REGISTRATION AND LOGIN)
router.post('/user/signup', validator.userSignup, CtrlUsersAuth.signup);                                 // --> SIGNUP
router.post('/user/signin', validator.userSignin, CtrlUsersAuth.signin);                                 // --> SINGIN

// USER ME (BY TOKEN AFTER AUTH)
router.get('/user/me', auth, validator.userMe, CtrlUsersMe.readMe);                                      // --> READ ME
router.patch('/user/me', auth, validator.userMe, validator.userUpdate, CtrlUsersMe.updateMe);            // --> UPDATE ME
router.delete('/user/me', auth, validator.userMe, validator.userDelete, CtrlUsersMe.deleteMe);           // --> DELETE ME

// USERS MANAGEMENT (CRUD FOR USERS CONTROL)
router.get('/users/list', CtrlUsers.findAll);                                                            // --> FIND ALL USERS
router.get('/users/find', CtrlUsers.findOne);                                                            // --> FIND A SPECIFIC USER
router.post('/users/create', CtrlUsers.create);                                                          // --> CREATE NEW USER
router.patch('/users/edit/:id', CtrlUsers.update);                                                           // --> EDIT A SPECIFIC USER
router.delete('/users/delete/:id', CtrlUsers.delete);                                                        // --> DELETE A SPECIFIC USER

// USERS GROUPS MANAGEMENT (CRUD FOR USER GROUPS CONTROL)
router.get('/usersgroups/list', CtrlUsersGroups.findAll);                                                // --> FIND ALL USER GROUPS
router.get('/usersgroups/find', CtrlUsersGroups.findOne);                                                // --> FIND A SPECIFIC USER GROUP
router.post('/usersgroups/create', CtrlUsersGroups.create);                                              // --> CREATE NEW USER GROUP
router.patch('/usersgroups/edit/:id', CtrlUsersGroups.update);                                               // --> EDIT A SPECIFIC USER GROUP
router.delete('/usersgroups/delete/:id', CtrlUsersGroups.delete);                                            // --> DELETE A SPECIFIC USER GROUP

module.exports = router;