require('rootpath')();

const moment = require('moment');
const colorTxt = require('ansi-colors');

const pkg = require('package.json');
const server = require('src/server/server');
const logger = require('src/utils/logging/winston/logger');

const startup = () => {
  console.clear();
  console.log(colorTxt.bgWhite.black(`\n Starting ${pkg.name.toUpperCase()} `) + colorTxt.bgMagenta.black(` v${pkg.version} `));
  console.log(colorTxt.white(`-> Api running in ${process.env.NODE_ENV} environment`));
  console.log(colorTxt.white(`-> Api started at ${moment().format('YYYY-MM-DD HH:mm')}`));

  logger.info(`Api starting ${pkg.name.toUpperCase()} v${pkg.version}`);
  logger.info(`Api running in ${process.env.NODE_ENV} environment`);
  logger.info(`Api started at ${moment().format('YYYY-MM-DD HH:mm')}`);

  server();
}

startup();