const neo4j = require('neo4j-driver');
const config = require('config/database');

const conf = config[process.env.NODE_ENV || 'development'];

const neo4jUrl = conf.neo4j.uri;
const neo4jAuth = neo4j.auth.basic(conf.neo4j.username, conf.neo4j.password);

module.exports = {
    neo4jUrl,
    neo4jAuth
}
