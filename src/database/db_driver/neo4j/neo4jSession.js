const neo4j = require("neo4j-driver");
const logger = require('src/utils/logging/winston/logger');
const neo4jConfig = require("src/database/db_driver/neo4j/neo4jConfig");

exports.executeCypherAsync = async (cql) => {

  try {
    let driver = neo4j.driver(
      neo4jConfig.neo4jUrl,
      neo4jConfig.neo4jAuth,
      { disableLosslessIntegers: true }
    );

    let session = driver.session();
    var result = await session.run(cql, null);

    session.close();
    driver.close();

    return result;
  } catch (err) {
    logger.error(`Database error. ${err}`);
    throw err;
  }
};
