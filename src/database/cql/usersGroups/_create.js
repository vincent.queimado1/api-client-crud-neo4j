module.exports = (datas) => {
    let query = `
    MATCH(n: Usersgroups)
    CREATE (p: Usersgroups {
        name: '${datas.name}'
    })
    RETURN (p)`;

    return query;
};