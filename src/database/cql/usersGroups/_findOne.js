module.exports = (datas) => {
    let condition = ''

    datas.id ? condition = `WHERE ID(u) = ${datas.id}` : ``;
    datas.name ? condition = `WHERE u.name = '${datas.name}'` : ``;
    datas.id && datas.name ? condition = `WHERE ID(u) = ${datas.id} OR u.name = '${datas.name}'` : ``;

    let query = `MATCH (u: Usersgroups) ${condition} RETURN (u)`;

    return query;
};