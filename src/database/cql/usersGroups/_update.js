module.exports = (id, datas) => {
    let newDatas = Object.keys(datas).map(key => {
        let u = `u.${key} = '${datas[key]}'`;
        return u;
      });

    let query = `
        MATCH (u: Usersgroups)
        WHERE ID(u) = ${id}
        SET ${newDatas}
        RETURN (u)`;
  
    return query;
};