const create = require('./_create')
const findAll = require('./_findAll')
const findOne = require('./_findOne')
const update = require('./_update')
const remove = require('./_delete')

module.exports = {
  create,
  findAll,
  findOne,
  update,
  remove
}
