module.exports = (id) => {
    let query = `
        MATCH (u: Users) 
        WHERE ID(u) = ${id}
        DETACH DELETE (u) 
        RETURN (u)`;
        
    return query;
};