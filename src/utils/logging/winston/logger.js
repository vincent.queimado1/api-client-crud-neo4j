require('winston-daily-rotate-file');
const winston = require('winston');
const logsPath = require('app-root-path').resolve('../logs')

console.log(logsPath)
const custom = winston.format.combine(
    winston.format.json(),
    winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
    winston.format.printf(info => `${info.timestamp} | ${info.level.toUpperCase()} | ${info.message}`)
)

const customInfo = winston.format.combine(
    winston.format.json(),
    winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
    winston.format.printf(info => `${info.timestamp} | ${info.level.toUpperCase()} | ${info.message}`)
)

const logger = winston.createLogger({
    transports: [
        new winston.transports.DailyRotateFile({
            frequency: '24h',
            name: 'error',
            level: 'error',
            dirname: `${logsPath}/`,
            filename: 'error-%DATE%.log',
            datePattern: 'YYYY-MM-DD-HH',
            handleExceptions: true,
            prepend: true,
            zippedArchive: false,
            maxSize: '5m',
            maxFiles: '7d',
            format: custom
        }),
        new winston.transports.DailyRotateFile({
            frequency: '24h',
            name: 'info',
            level: 'info',
            dirname: `${logsPath}/`,
            filename: 'info-%DATE%.log',
            datePattern: 'YYYY-MM-DD-HH',
            prepend: true,
            zippedArchive: false,
            maxSize: '5m',
            maxFiles: '7d',
            format: customInfo
        }),
        new winston.transports.Console({
            format: winston.format.combine(winston.format.timestamp(), custom),
            level: 'error',
            handleExceptions: true
        })
    ],
    exitOnError: false
})

logger.stream = {
    write: function(message, encoding) {
        logger.info(message)
    }
}

module.exports = logger