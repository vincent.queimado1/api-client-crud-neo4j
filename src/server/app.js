const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const express = require('express');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');

const routes = require('src/routes/default');
const apiRoutes = require('src/routes/routes');

const config = require('config/server');
const logger = require('src/utils/logging/winston/logger');
const handleError = require('src/utils/http_handler/error_handler');

const conf = config[process.env.NODE_ENV || 'development'];
const publicAssets = './public';
const publicFavicon = 'public/assets/images/favicons/favicon.png';

module.exports = () => {
  let app = express();

  if (conf.debug.http_request) app.use(morgan('HTTP request from :remote-addr :method :url :status :res[content-length] - :response-time ms', { stream: { write: message => logger.info(message.trim()) }} ));
  if (conf.debug.http_request) app.use(morgan('dev'));

  app.use(helmet());

  app.use(cors());

  app.use(favicon(publicFavicon));
  app.use('/static', express.static(publicAssets, { dotfiles: 'allow' }));

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
 
  app.use('/', routes);
  app.use('/api/v1/', apiRoutes);

  app.get("*", (req, res, next) => {
    next({ 'error': 'NotFound' });
  })

  app.use(handleError);

  return app;
}