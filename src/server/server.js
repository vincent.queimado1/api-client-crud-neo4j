const http = require('http');
const https = require('https');
const colorTxt = require('ansi-colors');

const app = require('src/server/app')();
const config = require('config/server');
const logger = require('src/utils/logging/winston/logger');

const conf = config[process.env.NODE_ENV || 'development'];

module.exports = async () => {
  let server = null;
  let serverSslOptions = null;
  let serverConnections = [];
  
  let serverHost = conf.server.host;
  let serverPort = normalizePort(conf.server.port);
  
  server = await createServer(app, serverSslOptions);

  server.listen(serverPort, serverHost);

  server.on('listening', () => onListening(serverHost, serverPort));
  server.on('error', (error) => onError(error, serverHost, serverPort, server, serverConnections));

  server.on('connection', connection => {
    serverConnections.push(connection);
    connection.on('close', () => serverConnections = serverConnections.filter(curr => curr !== connection));
  })

  if (conf.debug.http_connection) getConnections(server, serverConnections);

  process.on('SIGTERM', () => shutDown(server, serverConnections));
  process.on('SIGINT', () => shutDown(server, serverConnections));

  return server;
}

const createServer = (app, serverSslOptions) => {
  let httpserver;

  conf.server.ssl === true ? httpserver = https.createServer(serverSslOptions, app) : httpserver = http.createServer(app);

  return httpserver;
}

const onListening = (host, port) => { 
  console.log(colorTxt.white(`-> Api status: ` + colorTxt.green(`Ready (listening on ${host}:${port})`)));
  logger.info(`Api status: Ready (listening on ${host}:${port})`);
}

const onError = (error, host, port, server, connections) => {
  if (error.syscall !== 'listen') throw error;

  switch (error.code) {
    case 'EACCES':
    console.log(`Http server error - Host ${host}:${port} requires elevated privileges (${error.code})`);
    logger.error(`Http server error - Host ${host}:${port} requires elevated privileges (${error.code})`);
      shutDown(server, connections);
      break;
    case 'EADDRINUSE':
    console.log(`Http server error - Host ${host}:${port} is already in use (${error.code})`);
    logger.error(`Http server error - Host ${host}:${port} is already in use (${error.code})`);
      shutDown(server, connections);
      break;
    case 'EADDRNOTAVAIL':
    console.log(`Http server error - Host ${host}:${port} not available (${error.code})`);
    logger.error(`Http server error - Host ${host}:${port} not available (${error.code})`);
      shutDown(server, connections);
      break;
    default:
      throw error;
  }
}

const shutDown = (server, connections) => {
  console.log(`Http server error - Received kill signal, shutting down gracefully (SHUTDOWN)`);
  logger.error(`Http server error - Received kill signal, shutting down gracefully (SHUTDOWN)`);
  server.close(() => {
    console.log(`Http server error - Closed out remaining connections (SHUTDOWN)`);
    logger.error(`Http server error - Closed out remaining connections (SHUTDOWN)`);
      process.exit(0);
  })

  setTimeout(() => {
    console.log(`Http server error - Could not close connections in time, forcefully shutting down (SHUTDOWN)`);
    logger.error(`Http server error - Could not close connections in time, forcefully shutting down (SHUTDOWN)`);
      process.exit(1);
  }, 10000)

  connections.forEach(curr => curr.end())
  setTimeout(() => connections.forEach(curr => curr.destroy()), 5000);
}

const normalizePort = (val) => {
  let port = parseInt(val, 10);
  if (isNaN(port)) { return val };
  if (port >= 0) { return port };
  return false;
}

const getConnections = (server) => {
   setInterval(() => server.getConnections(
      (err, serverConnections) => {
        console.log(`${serverConnections} connections currently open`);
        if(err) console.log(`Http server error - ${err}`);
      }
  ), 1000)
}
