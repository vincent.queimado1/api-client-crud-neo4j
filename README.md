![Semantic description of image](/public/readme/title.png "Title")

# API - Client CRUD with NEO4J Graph database

## Sobre a API

API de cadastro de Clientes (CRUD).
A API foi desenvolvido a partir de NodeJs utilizando o framework Express e banco de dados de grafos Neo4j. Alguns recursos básicos foram inplementados como a autenticação JWT, validações e testes unitários.

Requisitos:

1. O cadastro dos clientes deve conter apenas seu nome, senha e endereço de e-mail;
2. Um cliente não pode se registrar duas vezes com o mesmo endereço de e-mail;
3. O acesso à api deve ser aberto ao mundo, porém deve possuir autenticação e autorização;

## Demo da API

A API é hospedada e executada na plataforma Heroku, e acessível no seguinte link:<br />
https://api-client-crud-neo4j.herokuapp.com/

## Packages utilizados

Lista de dependencias utilizadas no projeto:<br />
ansi-colors, bcrypt, cors, cross-env, dotenv-safe, express, helmet, jest, joi, jsonwebtoken, moment, morgan, neo4j-driver, nodemon, rootpath, serve-favicon

## Escopo da estrutura

Resumo de estrutura da API:

```
config\                 # Environment variables and configuration related things
docs\                   # Documentation / Postman collection
node_modules\           # Node modules
public\                 # Public ressources
src\                    # Sources
 |--api\                # APi - Custom MVC with presenters/services
     |--controllers\    # Controllers
     |--models\         # Models
     |--presenters\     # Presenters
     |--services\       # Services
 |--database\           # Database - Neo4j Graph database driver and cyphers script files (CQL)
 |--middlewares\        # Middlewares (auth and validation)
 |--server\             # Http Server (with Express framework)
 |--routes\             # Custom Routes
 |--utils\              # Utility handler, special classes or functions
 |--main.js             # App entry point
test\                   # Unit test files (Jest)
```

## Instruções de instalação e execução local

Antes de executar o projeto localmente, é necessário criar uma conta na NEO4J e configurar os dados de acesso ao banco de grafo nas variveis de ambiente (arquivo .env).

Link de criação de banco de grafos Neo4J: https://neo4j.com/cloud/aura/?ref=neo4j-home-hero

Para realizar a instalação local do projeto, segue o passo a passo abaixo:

Clonar o projeto a partir do repositório Gitlab:

```bash
  git clone https://gitlab.com/vincent.queimado1/api-client-crud-neo4j.git
  cd api-client-crud
```

Instalação de dependências:

```bash
  npm install
```

Configuração de variavéis de ambiente:

```bash
cp .env.example .env
# abrir o arquivo .env e alterar as variaveis de ambiente da API e acesso ao banco de dados
```

![Semantic description of image](/public/readme/env.png "Env file")

Executando localmente em ambiente de desenvolvimento:

```bash
  npm run dev
```

![Semantic description of image](/public/readme/running.jpg "Running")

Executando em ambiente de produção:

```bash
  npm run start
```

Ao executar localmente, a API será acessível por padrão na url e porta: http://localhost:8080

## Documentação de API:

Segue o link de documentação de API para realização de requisições. Na pasta dos do projeto encontra-se a colleção de requisições do projeto para testes.<br />
Link: https://documenter.getpostman.com/view/6300836/UVyyuYjw

Tipos de requisições (Auth + CRUD Cliente):

- Signup (CREATE - Cadastro de novo cliente):<br />
  POST https://api-client-crud-neo4j.herokuapp.com/api/v1/client/signup

- Signin (Autenticação e obtenção de token de cliente para demais requisições):<br />
  POST https://api-client-crud-neo4j.herokuapp.com/api/v1/client/signin

- Show Me (READ - Leitura de dados do cliente - Apenas com token de cliente logado):<br />
  GET https://api-client-crud-neo4j.herokuapp.com/api/v1/client/me

- Update Me (UPDATE - Modificação de dados do cliente - Apenas com token de cliente logado):<br />
  PATCH https://api-client-crud-neo4j.herokuapp.com/api/v1/client/me

- Delete Me (DELETE - Exlusão do cliente - Apenas com token de cliente logado):<br />
  DELETE https://api-client-crud-neo4j.herokuapp.com/api/v1/client/me

Requisições Extras:

- Api Info:<br />
  GET https://api-client-crud-neo4j.herokuapp.com/api/v1/info

- Api Version:<br />
  GET https://api-client-crud-neo4j.herokuapp.com/api/v1/version
