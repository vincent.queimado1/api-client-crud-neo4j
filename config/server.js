if (process.env.NODE_ENV !== 'production') require ('dotenv-safe').config({ silent: true, allowEmptyValues: true })

module.exports = {
  'development': {
    'server': {
      'host': process.env.API_URI || 'localhost',
      'port': process.env.API_PORT || 8080,
      'ssl' : process.env.API_SSL || false,
    },
    'session': {
      'secret': process.env.API_SECRET || 'secret',
      'rounds': process.env.API_ROUNDS || 10,
    },
    'jwt': {
      'expiredIn': process.env.APP_JWT_EXPIRED_IN || "24h"
    },
    'debug': {
      'http_request': process.env.DEBUG_HTTP_REQUEST || true,
      'http_connection': process.env.DEBUG_CONNECTION || false
    }
  },
  'staging': {
      'server': {
        'host': process.env.API_URI || 'localhost',
        'port': process.env.API_PORT || 8080,
        'ssl' : process.env.API_SSL || false,
      },
      'session': {
        'secret': process.env.API_SECRET || 'secret',
        'rounds': process.env.API_ROUNDS || 10,
      },
      'jwt': {
        'expiredIn': process.env.APP_JWT_EXPIRED_IN || "24h"
      },
      'debug': {
        'http_request': process.env.DEBUG_HTTP_REQUEST || true,
        'http_connection': process.env.DEBUG_CONNECTION || false
      }
  },
  'production': {
      'server': {
        'host': process.env.HOST || process.env.API_PHOST || '0.0.0.0',
        'port': process.env.PORT || process.env.API_PORT || 8080,
        'ssl' : process.env.API_SSL || false,
      },
      'session': {
        'secret': process.env.API_SECRET || 'secret',
        'rounds': process.env.API_ROUNDS || 10,
      },
      'jwt': {
        'expiredIn': process.env.APP_JWT_EXPIRED_IN || "24h"
      },
      'debug': {
        'http_request': process.env.DEBUG_HTTP_REQUEST || true,
        'http_connection': process.env.DEBUG_CONNECTION || false
      }
  }
}