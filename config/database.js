if (process.env.NODE_ENV !== 'production') require ('dotenv-safe').config({ silent: true, allowEmptyValues: true })

module.exports = {
  'development': {
    'neo4j': {
      'uri': process.env.NEO4J_URI || '',
      'username': process.env.NEO4J_USERNAME || '',
      'password': process.env.NEO4J_PASSWORD || '',
      'databae': process.env.NEO4J_DATABASE || '',
    }
  },
  'staging': {
    'neo4j': {
      'uri': process.env.NEO4J_URI || '',
      'username': process.env.NEO4J_USERNAME || '',
      'password': process.env.NEO4J_PASSWORD || '',
      'databae': process.env.NEO4J_DATABASE || '',
      }
  },
  'production': {
    'neo4j': {
      'uri': process.env.NEO4J_URI || '',
      'username': process.env.NEO4J_USERNAME || '',
      'password': process.env.NEO4J_PASSWORD || '',
      'databae': process.env.NEO4J_DATABASE || '',
      }
  }
}